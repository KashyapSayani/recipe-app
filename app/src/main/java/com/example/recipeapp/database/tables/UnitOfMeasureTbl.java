package com.example.recipeapp.database.tables;

import android.content.Context;

import com.example.recipeapp.database.MyDatabase;

public class UnitOfMeasureTbl extends MyDatabase {

    public static final String TABLE_NAME = "UnitOfMeasure";
    public static final String UNIT_OF_MEASURE_ID = "UnitOfMeasureId";
    public static final String UNIT_OF_MEASURE_NAME = "UnitOfMeasureName";

    public UnitOfMeasureTbl(Context context) {
        super(context);
    }
}
