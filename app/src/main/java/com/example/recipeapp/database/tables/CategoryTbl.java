package com.example.recipeapp.database.tables;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.recipeapp.database.MyDatabase;
import com.example.recipeapp.model.CategoryModel;

import java.util.ArrayList;

public class CategoryTbl extends MyDatabase {

    public static final String TABLE_NAME = "Category";
    public static final String CATEGORY_ID = "CategoryId";
    public static final String CATEGORY_NAME = "CategoryName";

    public CategoryTbl(Context context) {
        super(context);
    }

    public ArrayList<CategoryModel> getCategories() {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<CategoryModel> categoryList = new ArrayList<>();
        String query = "SELECT * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();

        for (int i = 0; i < cursor.getCount(); i++) {
            CategoryModel categoryModel = new CategoryModel();

            categoryModel.setCategoryId(cursor.getInt(cursor.getColumnIndex(CATEGORY_ID)));
            categoryModel.setCategoryName(cursor.getString(cursor.getColumnIndex(CATEGORY_NAME)));
            categoryList.add(categoryModel);
            cursor.moveToNext();
        }

        cursor.close();
        db.close();

        return categoryList;
    }
}
