package com.example.recipeapp.database.tables;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.recipeapp.database.MyDatabase;
import com.example.recipeapp.model.IngredientModel;

import java.util.ArrayList;

public class IngredientTbl extends MyDatabase {

    public static final String TABLE_NAME = "Ingredient";
    public static final String INGREDIENT_ID = "IngredientId";
    public static final String INGREDIENT_NAME = "IngredientName";

    public IngredientTbl(Context context) {
        super(context);
    }

    public ArrayList<IngredientModel> getIngredients() {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<IngredientModel> ingredientList = new ArrayList<>();
        String query = "SELECT * FROM " + TABLE_NAME + " ORDER BY " + INGREDIENT_NAME + " ASC ";
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();

        for (int i = 0; i < cursor.getCount(); i++) {

            IngredientModel ingredient = new IngredientModel();
            ingredient.setIngredientId(cursor.getInt(cursor.getColumnIndex(INGREDIENT_ID)));
            ingredient.setIngredientName(cursor.getString(cursor.getColumnIndex(INGREDIENT_NAME)));
            ingredientList.add(ingredient);
            cursor.moveToNext();
        }

        cursor.close();
        db.close();

        return ingredientList;
    }
}