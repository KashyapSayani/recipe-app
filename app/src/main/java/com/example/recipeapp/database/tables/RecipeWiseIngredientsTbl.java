package com.example.recipeapp.database.tables;

import android.content.Context;

import com.example.recipeapp.database.MyDatabase;

public class RecipeWiseIngredientsTbl extends MyDatabase {

    public static final String TABLE_NAME = "RecipeWiseIngredients";
    public static final String RECIPE_ID = "RecipeId";
    public static final String INGREDIENT_ID = "IngredientId";
    public static final String UNIT_OF_MEASURE_ID = "UnitOfMeasureId";
    public static final String UNIT_OF_MEASURE_QUANTITY = "UnitOfMeasureQuantity";


    public RecipeWiseIngredientsTbl(Context context) {
        super(context);
    }
}
