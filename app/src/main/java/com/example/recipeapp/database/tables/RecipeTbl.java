package com.example.recipeapp.database.tables;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.recipeapp.database.MyDatabase;
import com.example.recipeapp.model.IngredientModel;
import com.example.recipeapp.model.RecipeDetailsModel;
import com.example.recipeapp.model.RecipeModel;

import java.util.ArrayList;

public class RecipeTbl extends MyDatabase {

    public static final String TABLE_NAME = "Recipe";
    public static final String RECIPE_ID = "RecipeId";
    public static final String RECIPE_NAME = "RecipeName";
    public static final String RECIPE_IMAGE = "RecipeImage";
    public static final String RECIPE_METHOD = "RecipeMethod";
    public static final String RECIPE_COOK_TIME = "RecipeCookTime";
    public static final String RECIPE_PREP_TIME = "RecipePrepTime";
    public static final String RECIPE_URL = "RecipeUrl";
    public static final String CATEGORY_ID = "CategoryId";
    public static final String UNIT_OF_MEASURE_NAME = "UnitOfMeasureName";
    public static final String UNIT_OF_MEASURE_QUANTITY = "UnitOfMeasureQuantity";
    public static final String INGREDIENT_NAME = "IngredientName";


    public RecipeTbl(Context context) {
        super(context);
    }

    public RecipeModel getCreatedModelUsingCursor(Cursor cursor) {
        RecipeModel recipe = new RecipeModel();

        recipe.setRecipeId(cursor.getInt(cursor.getColumnIndex(RECIPE_ID)));
        recipe.setRecipeName(cursor.getString(cursor.getColumnIndex(RECIPE_NAME)));
        recipe.setRecipeImage(cursor.getString(cursor.getColumnIndex(RECIPE_IMAGE)));
        recipe.setRecipeMethod(cursor.getString(cursor.getColumnIndex(RECIPE_METHOD)));
        recipe.setRecipeCookTime(cursor.getString(cursor.getColumnIndex(RECIPE_COOK_TIME)));
        recipe.setRecipePrepTime(cursor.getString(cursor.getColumnIndex(RECIPE_PREP_TIME)));
        recipe.setRecipeUrl(cursor.getString(cursor.getColumnIndex(RECIPE_URL)));
        recipe.setCategoryId(cursor.getInt(cursor.getColumnIndex(CATEGORY_ID)));

        return recipe;
    }

    public ArrayList<RecipeModel> getRecipes() {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<RecipeModel> recipeModelList = new ArrayList<>();
        String query = "SELECT * FROM " + TABLE_NAME + " ORDER BY " + RECIPE_NAME + " ASC ";
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();

        for (int i = 0; i < cursor.getCount(); i++) {
            recipeModelList.add(getCreatedModelUsingCursor(cursor));
            cursor.moveToNext();
        }

        cursor.close();
        db.close();
        return recipeModelList;
    }

    public ArrayList<IngredientModel> getRecipeWiseIngredient(int id) {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<IngredientModel> ingredientList = new ArrayList<>();
        String query = "SELECT Ingredient.IngredientId,Ingredient.IngredientName " +
                "FROM Recipe " +
                "INNER JOIN RecipeWiseIngredients " +
                "ON Recipe.RecipeId = RecipeWiseIngredients.RecipeId " +
                "INNER JOIN Ingredient " +
                "ON RecipeWiseIngredients.IngredientId = Ingredient.IngredientId " +
                "WHERE Recipe.RecipeId=?";
        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(id)});

        cursor.moveToFirst();

        for (int i = 0; i < cursor.getCount(); i++) {
            IngredientModel ingredient = new IngredientModel();
            ingredient.setIngredientId(cursor.getInt(cursor.getColumnIndex(IngredientTbl.INGREDIENT_ID)));
            ingredient.setIngredientName(cursor.getString(cursor.getColumnIndex(IngredientTbl.INGREDIENT_NAME)));
            ingredientList.add(ingredient);
            cursor.moveToNext();
        }

        cursor.close();
        db.close();

        return ingredientList;
    }

    public RecipeModel getRecipeById(int id) {
        SQLiteDatabase db = getReadableDatabase();
        RecipeModel recipe;
        String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + RECIPE_ID + " = ? ";
        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(id)});

        cursor.moveToFirst();
        recipe = getCreatedModelUsingCursor(cursor);

        cursor.close();
        db.close();

        return recipe;
    }

    public RecipeDetailsModel getRecipeDetailsByRecipeId(int recipeId) {
        SQLiteDatabase db = getReadableDatabase();
        RecipeDetailsModel recipeDetailsModel = new RecipeDetailsModel();

        String query = "SELECT Recipe.RecipeName, Recipe.RecipePrepTime, Recipe.RecipeCookTime ,Ingredient.IngredientName , " +
                "RecipeWiseIngredients.UnitOfMeasureQuantity , UnitOfMeasure.UnitOfMeasureName ,Recipe.RecipeMethod " +
                "FROM RecipeWiseIngredients INNER JOIN Recipe ON RecipeWiseIngredients.RecipeId = Recipe.RecipeId " +
                "INNER JOIN Ingredient " +
                "ON RecipeWiseIngredients.IngredientId = Ingredient.IngredientId " +
                "INNER JOIN UnitOfMeasure " +
                "ON RecipeWiseIngredients.UnitOfMeasureId = UnitOfMeasure.UnitOfMeasureId " +
                "WHERE Recipe.RecipeId = ?";

        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(recipeId)});

        cursor.moveToFirst();

        recipeDetailsModel.setRecipeName(cursor.getString(cursor.getColumnIndex(RECIPE_NAME)));
        recipeDetailsModel.setRecipePrepTime(cursor.getString(cursor.getColumnIndex(RECIPE_PREP_TIME)));
        recipeDetailsModel.setRecipeCookTime(cursor.getString(cursor.getColumnIndex(RECIPE_COOK_TIME)));
        recipeDetailsModel.setRecipeMethod(cursor.getString(cursor.getColumnIndex(RECIPE_METHOD)));

        ArrayList<String> unitOfMeasureName = new ArrayList<>();
        ArrayList<String> unitOfMeasureQuantity = new ArrayList<>();
        ArrayList<String> recipeWiseIngredient = new ArrayList<>();

        for (int i = 0; i < cursor.getCount(); i++) {

            unitOfMeasureName.add(cursor.getString(cursor.getColumnIndex(UNIT_OF_MEASURE_NAME)));
            unitOfMeasureQuantity.add(cursor.getString(cursor.getColumnIndex(UNIT_OF_MEASURE_QUANTITY)));
            recipeWiseIngredient.add(cursor.getString(cursor.getColumnIndex(INGREDIENT_NAME)));

            cursor.moveToNext();
        }

        recipeDetailsModel.setUnitOfMeasureName(unitOfMeasureName);
        recipeDetailsModel.setUnitOfMeasureQuantity(unitOfMeasureQuantity);
        recipeDetailsModel.setRecipeWiseIngredient(recipeWiseIngredient);

        cursor.close();
        db.close();

        return recipeDetailsModel;
    }
}
