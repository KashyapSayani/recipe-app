package com.example.recipeapp.activity;

import android.os.Build;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.recipeapp.R;
import com.example.recipeapp.model.RecipeDetailsModel;
import com.example.recipeapp.recycler_view.RecipeIngredientRecyclerView;

public class RecipeDetailsActivity extends BaseActivity {

    TextView tvRecipeName;
    TextView tvPrepTime;
    TextView tvCookTime;
    WebView rcvRecipeMethod;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_details);
        RecipeDetailsModel  recipeDetailsModel = (RecipeDetailsModel) getIntent().getSerializableExtra("RecipeDetails");
        setTittleOfToolbar(recipeDetailsModel.getRecipeName());
        initViewReference();
        setRecipeDetails();
        buildRecyclerView();
    }

    public void initViewReference() {
        tvRecipeName = findViewById(R.id.tvRecipeName);
        tvPrepTime = findViewById(R.id.tvPrepTime);
        tvCookTime = findViewById(R.id.tvCookTime);
        rcvRecipeMethod = findViewById(R.id.rcvRecipeMethod);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void setRecipeDetails() {
        if (getIntent().getExtras() != null) {

            RecipeDetailsModel recipeDetailsModel = (RecipeDetailsModel) getIntent().getSerializableExtra("RecipeDetails");
            tvRecipeName.setText(recipeDetailsModel.getRecipeName());
            tvPrepTime.setText(recipeDetailsModel.getRecipePrepTime());
            tvCookTime.setText(recipeDetailsModel.getRecipeCookTime());

            rcvRecipeMethod.loadDataWithBaseURL("",recipeDetailsModel.getRecipeMethod(),"text/html","UTF-8","");
        }
    }

    public void buildRecyclerView() {

        if (getIntent().getExtras() != null) {

            RecipeDetailsModel recipeDetailsModel = (RecipeDetailsModel) getIntent().getSerializableExtra("RecipeDetails");

            RecyclerView rcvRecipeDetails = findViewById(R.id.rcvRecipeIngredient);
            RecyclerView.LayoutManager rcvRecipeDetailsLayoutManager = new LinearLayoutManager(this);
            RecipeIngredientRecyclerView recipeIngredientRecyclerView = new RecipeIngredientRecyclerView(recipeDetailsModel.getRecipeWiseIngredient(), recipeDetailsModel.getUnitOfMeasureName(), recipeDetailsModel.getUnitOfMeasureQuantity());

            rcvRecipeDetails.setLayoutManager(rcvRecipeDetailsLayoutManager);
            rcvRecipeDetails.setAdapter(recipeIngredientRecyclerView);
        }
    }
}