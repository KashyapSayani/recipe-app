package com.example.recipeapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.recipeapp.R;
import com.example.recipeapp.database.tables.IngredientTbl;
import com.example.recipeapp.model.IngredientModel;
import com.example.recipeapp.recycler_view.SearchRecyclerView;
import com.example.recipeapp.util.Constant;

import java.util.ArrayList;

public class SearchIngredientActivity extends BaseActivity {

    private EditText etSearchBox;
    private RecyclerView ingredientRecyclerView;
    private SearchRecyclerView searchRecyclerView;
    private RecyclerView.LayoutManager ingredientLayoutManager;
    private ArrayList<IngredientModel> userIngredientList = new ArrayList<>();
    private ArrayList<IngredientModel> tempUserIngredientFilterList;
    private TextView noIngredientFound;
    ArrayList<IngredientModel> tempIngredientList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_ingredient);
        initViewRefrence();
        buildRecyclerViewOfIngredient();
        search();
        setTittleOfToolbar(getString(R.string.str_ingredients));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.app_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==R.id.action_refresh){
            Intent intent = new Intent(this,RecipeSuggestionActivity.class);
            intent.putExtra(Constant.USER_INGREDIENT_LIST,userIngredientList);
            startActivity(intent);
        }

        return true;
    }

    void initViewRefrence() {
        etSearchBox = findViewById(R.id.etSearchBox);
        noIngredientFound = findViewById(R.id.tvNoIngredientFound);
    }

    void search() {
        etSearchBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });
    }

    void filter(String text) {

        ArrayList<IngredientModel> filteredList = new ArrayList<>();
        ArrayList<IngredientModel> ingredientList = tempIngredientList;

        for (IngredientModel ingredient : ingredientList) {
            if (ingredient.getIngredientName().toLowerCase().contains(text.trim().toLowerCase())) {
                filteredList.add(ingredient);
            }

            searchRecyclerView.filterList(filteredList);
            tempUserIngredientFilterList = filteredList;

            checkVisibilityOfIngredient(filteredList);
        }
    }

    private void buildRecyclerViewOfIngredient() {

        tempIngredientList = new IngredientTbl(SearchIngredientActivity.this).getIngredients();

        ingredientRecyclerView = findViewById(R.id.rcvRecipeSearchSuggestionList);
        ingredientLayoutManager = new GridLayoutManager(this, 3);
        searchRecyclerView = new SearchRecyclerView(tempIngredientList, new SearchRecyclerView.OnClickListener() {
            @Override
            public void onImageView1Click(int position) {

                if (tempUserIngredientFilterList == null) {
                    if (!userIngredientList.contains(tempIngredientList.get(position))) {
                        userIngredientList.add(tempIngredientList.get(position));
                        tempIngredientList.get(position).setSelectedOrNot(1);
                        searchRecyclerView.notifyDataSetChanged();
                    }
                } else {
                    if (!userIngredientList.contains(tempUserIngredientFilterList.get(position))) {
                        userIngredientList.add(tempUserIngredientFilterList.get(position));
                        tempUserIngredientFilterList.get(position).setSelectedOrNot(1);
                        searchRecyclerView.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onImageView2Click(int position) {

                if (tempUserIngredientFilterList == null) {
                    if (userIngredientList.contains(tempIngredientList.get(position))) {
                        userIngredientList.remove(tempIngredientList.get(position));
                        tempIngredientList.get(position).setSelectedOrNot(0);
                        searchRecyclerView.notifyDataSetChanged();
                    }
                } else {
                    if (userIngredientList.contains(tempUserIngredientFilterList.get(position))) {
                        userIngredientList.remove(tempUserIngredientFilterList.get(position));
                        tempUserIngredientFilterList.get(position).setSelectedOrNot(0);
                        searchRecyclerView.notifyDataSetChanged();
                    }
                }
            }
        });

        ingredientRecyclerView.setLayoutManager(ingredientLayoutManager);
        ingredientRecyclerView.setAdapter(searchRecyclerView);
    }

    public void checkVisibilityOfIngredient(ArrayList<IngredientModel> filteredList) {
        if (filteredList.size() == 0) {
            noIngredientFound.setVisibility(View.VISIBLE);
            ingredientRecyclerView.setVisibility(View.GONE);
        } else {
            noIngredientFound.setVisibility(View.GONE);
            ingredientRecyclerView.setVisibility(View.VISIBLE);
        }
    }

}