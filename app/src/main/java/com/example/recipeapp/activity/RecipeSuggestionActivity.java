package com.example.recipeapp.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.recipeapp.R;
import com.example.recipeapp.database.tables.RecipeTbl;
import com.example.recipeapp.model.IngredientModel;
import com.example.recipeapp.model.RecipeDetailsModel;
import com.example.recipeapp.model.RecipeModel;
import com.example.recipeapp.recycler_view.SuggestedRecipeList1RecyclerView;
import com.example.recipeapp.recycler_view.SuggestedRecipeList2RecyclerView;
import com.example.recipeapp.util.Constant;

import java.util.ArrayList;

public class RecipeSuggestionActivity extends AppCompatActivity {

    private ArrayList<RecipeModel> suggestedRecipeList1 = new ArrayList<>();
    private ArrayList<RecipeModel> suggestedRecipeList2 = new ArrayList<>();
    private ArrayList<IngredientModel> userIngredientList = new ArrayList<>();
    RecyclerView rcvSuggestedRecipeList1;
    RecyclerView rcvSuggestedRecipeList2;
    TextView tvNoRecipeFoundList1;
    TextView tvNoRecipeFoundList2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_suggestion);
        initViewRefrence();
        getIngredientWiseRecipe();
    }

    public void initViewRefrence() {
        rcvSuggestedRecipeList1 = findViewById(R.id.rcvSuggestedRecipeList1);
        rcvSuggestedRecipeList2 = findViewById(R.id.rcvSuggestedRecipeList2);
        tvNoRecipeFoundList1 = findViewById(R.id.tvNoRecipeFoundList1);
        tvNoRecipeFoundList2 = findViewById(R.id.tvNoRecipeFoundList2);

    }

    public void getIngredientWiseRecipe() {
        if (getIntent().hasExtra(Constant.USER_INGREDIENT_LIST)) {
            ArrayList<RecipeModel> tempRecipe = new RecipeTbl(this).getRecipes();
            int totalRecipe = tempRecipe.size();

            for (int i = 1; i <= totalRecipe; i++) {
                ArrayList<IngredientModel> recipeWiseIngredient = new RecipeTbl(RecipeSuggestionActivity.this).getRecipeWiseIngredient(i);
                findRecipeSuggesstion(recipeWiseIngredient, i);
            }
        }
    }

    @SuppressLint("LongLogTag")
    public void findRecipeSuggesstion(ArrayList<IngredientModel> recipeWiseIngredient, int i) {

        userIngredientList = (ArrayList<IngredientModel>) getIntent().getSerializableExtra(Constant.USER_INGREDIENT_LIST);


        int beforeSize = recipeWiseIngredient.size();
        int countRecipeList1 = 0;
        int countRecipeList2 = 0;

        for (int j = 0; j < userIngredientList.size(); j++) {
            for (int k = 0; k < recipeWiseIngredient.size(); k++) {
                if (recipeWiseIngredient.get(k).getIngredientName().toLowerCase().trim().equals(userIngredientList.get(j).getIngredientName().toLowerCase().trim())) {
                    recipeWiseIngredient.remove(k);
                }
            }
        }
        int afterSize = recipeWiseIngredient.size();

        String remaingIngredients = null;

        if (afterSize == 0) {
            suggestedRecipeList1.add(new RecipeTbl(this).getRecipeById(i));
            countRecipeList1++;
            buildRecyclerViewForRecipeList1(suggestedRecipeList1, countRecipeList1);
        } else if (afterSize <= beforeSize / 2) {
            countRecipeList2++;
            suggestedRecipeList2.add(new RecipeTbl(this).getRecipeById(i));

            for (int j = 0; j < recipeWiseIngredient.size(); j++) {
                remaingIngredients = remaingIngredients + "," + recipeWiseIngredient.get(j).getIngredientName();
            }
            remaingIngredients = remaingIngredients.substring(remaingIngredients.indexOf(",") + 1);

            buildRecyclerViewForRecipeList2(suggestedRecipeList2, remaingIngredients, countRecipeList2);
        }

        checkVisibility(suggestedRecipeList1, suggestedRecipeList2);

    }

    public void buildRecyclerViewForRecipeList1(ArrayList<RecipeModel> recipeList, int countRecipeList1) {

        RecyclerView rcvSuggestedRecipeList1 = findViewById(R.id.rcvSuggestedRecipeList1);
        RecyclerView.LayoutManager recipeList1LayoutManager = new GridLayoutManager(this,3);
        SuggestedRecipeList1RecyclerView suggestedRecipeList1RecyclerView = new SuggestedRecipeList1RecyclerView(recipeList, new SuggestedRecipeList1RecyclerView.OnClickListener() {
            @Override
            public void onItemClick(int position) {
                RecipeDetailsModel recipeDetailsModel = new RecipeTbl(RecipeSuggestionActivity.this).getRecipeDetailsByRecipeId(recipeList.get(position).getRecipeId());

                Intent intent = new Intent(RecipeSuggestionActivity.this, RecipeDetailsActivity.class);
                intent.putExtra("RecipeDetails",recipeDetailsModel);
                startActivity(intent);
            }
        });

        if (countRecipeList1 == 1) {
            rcvSuggestedRecipeList1.setLayoutManager(recipeList1LayoutManager);
            rcvSuggestedRecipeList1.setAdapter(suggestedRecipeList1RecyclerView);
        } else {
            suggestedRecipeList1RecyclerView.filterRecipeList(recipeList);
        }
    }

    public void buildRecyclerViewForRecipeList2(ArrayList<RecipeModel> recipeList, String remaingIngredients, int countRecipeList2) {
        RecyclerView rcvSuggestedRecipeList2 = findViewById(R.id.rcvSuggestedRecipeList2);
        RecyclerView.LayoutManager recipeList2LayoutManager = new GridLayoutManager(this,3);
        SuggestedRecipeList2RecyclerView suggestedRecipeList2RecyclerView = new SuggestedRecipeList2RecyclerView(recipeList, remaingIngredients, new SuggestedRecipeList2RecyclerView.OnClickListener() {
            @Override
            public void onItemClick(int position) {
                RecipeDetailsModel recipeDetailsModel = new RecipeTbl(RecipeSuggestionActivity.this).getRecipeDetailsByRecipeId(recipeList.get(position).getRecipeId());

                Intent intent = new Intent(RecipeSuggestionActivity.this, RecipeDetailsActivity.class);
                intent.putExtra("RecipeDetails",recipeDetailsModel);
                startActivity(intent);
            }
        });

        if (countRecipeList2 == 1) {
            rcvSuggestedRecipeList2.setLayoutManager(recipeList2LayoutManager);
            rcvSuggestedRecipeList2.setAdapter(suggestedRecipeList2RecyclerView);
        } else {
            suggestedRecipeList2RecyclerView.filterRecipeList(recipeList);
        }
    }

    public void checkVisibility(ArrayList<RecipeModel> list1, ArrayList<RecipeModel> list2) {
        if (list1.size() == 0) {
            tvNoRecipeFoundList1.setVisibility(View.VISIBLE);
            rcvSuggestedRecipeList1.setVisibility(View.GONE);
        } else {
            tvNoRecipeFoundList1.setVisibility(View.GONE);
            rcvSuggestedRecipeList1.setVisibility(View.VISIBLE);
        }

        if (list2.size() == 0) {
            tvNoRecipeFoundList2.setVisibility(View.VISIBLE);
            rcvSuggestedRecipeList2.setVisibility(View.GONE);
        } else {
            tvNoRecipeFoundList2.setVisibility(View.GONE);
            rcvSuggestedRecipeList2.setVisibility(View.VISIBLE);
        }
    }
}