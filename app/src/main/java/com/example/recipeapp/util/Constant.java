package com.example.recipeapp.util;

public class Constant {

    public static final String USER_INGREDIENT_LIST = "UserIngredientList";
    public static final String RECIPE_NAME = "RecipeName";
    public static final String RECIPE_PREPRETION_TIME = "RecipePreprationTime";
    public static final String RECIPE_COOKING_TIME = "Recipe_Cooking_Time";
    public static final String RECIPE_INGREDIENT_LIST = "Recipe_Ingredient_List";
    public static final String RECIPE_METHOD = "Recipe_Method";
    public static final String RECIPE_UNIT_OF_MEASURE_NAME = "RecipeUnitOfMeasureName";
    public static final String RECIPE_UNIT_OF_MEASURE_QUANTITY = "RecipeUnitOfMeasureQuantity";
}
