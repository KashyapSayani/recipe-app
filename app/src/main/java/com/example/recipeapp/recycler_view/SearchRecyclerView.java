package com.example.recipeapp.recycler_view;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.recipeapp.R;
import com.example.recipeapp.model.IngredientModel;

import java.util.ArrayList;

public class SearchRecyclerView extends RecyclerView.Adapter<SearchRecyclerView.SearchViewHolder> {

    private ArrayList<IngredientModel> ingredientList;
    private OnClickListener onClickListener;

    public SearchRecyclerView(ArrayList<IngredientModel> ingredientList, OnClickListener onClickListener) {
        this.ingredientList = ingredientList;
        this.onClickListener = onClickListener;
    }

    @NonNull
    @Override
    public SearchViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.ingredient_list,
                parent, false);
        SearchViewHolder searchViewHolder = new SearchViewHolder(v);
        return searchViewHolder;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull SearchViewHolder holder, int position) {
        IngredientModel currentItem = ingredientList.get(position);
        holder.ingredient.setText(currentItem.getIngredientName());
        if (currentItem.getSelectedOrNot() == 0) {
            holder.imgvIngredient2.setVisibility(View.GONE);
        } else {
            holder.imgvIngredient2.setVisibility(View.VISIBLE);
        }

        holder.imgvIngredient1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (onClickListener != null) {
                    onClickListener.onImageView1Click(position);
                }
            }
        });

        holder.imgvIngredient2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onClickListener != null) {
                    onClickListener.onImageView2Click(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return ingredientList.size();
    }

    public void filterList(ArrayList<IngredientModel> filteredList) {
        ingredientList = filteredList;
        notifyDataSetChanged();
    }

    public interface OnClickListener {
        void onImageView1Click(int position);

        void onImageView2Click(int position);
    }

    public static class SearchViewHolder extends RecyclerView.ViewHolder {

        TextView ingredient;
        FrameLayout layout;
        ImageView imgvIngredient1;
        ImageView imgvIngredient2;

        public SearchViewHolder(@NonNull View itemView) {
            super(itemView);
            ingredient = itemView.findViewById(R.id.tvIngredient);
            layout = itemView.findViewById(R.id.layout);
            imgvIngredient1 = itemView.findViewById(R.id.imgvIngredient1);
            imgvIngredient2 = itemView.findViewById(R.id.imgvIngredient2);
        }
    }
}