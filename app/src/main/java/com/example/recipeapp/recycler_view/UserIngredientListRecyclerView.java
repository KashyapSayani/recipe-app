package com.example.recipeapp.recycler_view;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.recipeapp.R;
import com.example.recipeapp.model.IngredientModel;

import java.util.ArrayList;

public class UserIngredientListRecyclerView extends RecyclerView.Adapter<UserIngredientListRecyclerView.ViewHolder> {

    ArrayList<IngredientModel> userIngredientList;
    OnClickListner onClickListner;

    boolean isFirst = true;

    public UserIngredientListRecyclerView(ArrayList<IngredientModel> userIngredientList, OnClickListner onClickListner) {
        this.userIngredientList = userIngredientList;
        this.onClickListner = onClickListner;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_ingredient_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        IngredientModel currentItem = userIngredientList.get(position);
        holder.userIngredient.setText(currentItem.getIngredientName());

        holder.userIngredient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickListner.onItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return userIngredientList.size();
    }

    public void filterListOfUserIngredient(ArrayList<IngredientModel> filteredList) {
        userIngredientList = filteredList;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView userIngredient;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            userIngredient = itemView.findViewById(R.id.tvUserIngredient);
        }
    }

    public interface OnClickListner {
        void onItemClick(int position);
    }
}