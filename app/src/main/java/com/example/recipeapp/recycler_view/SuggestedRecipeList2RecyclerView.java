package com.example.recipeapp.recycler_view;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.recipeapp.R;
import com.example.recipeapp.model.RecipeModel;

import java.util.ArrayList;

public class SuggestedRecipeList2RecyclerView extends RecyclerView.Adapter<SuggestedRecipeList2RecyclerView.RecipeList2ViewHolder> {

    ArrayList<RecipeModel> recipeList2;
    String remaingIngredients;
    OnClickListener onClickListener;

    public SuggestedRecipeList2RecyclerView(ArrayList<RecipeModel> recipeList2, String remaingIngredients, OnClickListener onClickListener) {
        this.recipeList2 = recipeList2;
        this.remaingIngredients = remaingIngredients;
        this.onClickListener = onClickListener;
    }

    @Override
    public void onBindViewHolder(@NonNull RecipeList2ViewHolder holder, int position) {
        RecipeModel currentItem = recipeList2.get(position);
        holder.suggestedRecipeList2.setText(currentItem.getRecipeName());
        holder.tvRemainingIngredients.setText(remaingIngredients);

        holder.imgvIngredientList2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickListener.onItemClick(position);
            }
        });

        holder.suggestedRecipeList2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickListener.onItemClick(position);
            }
        });
    }

    @NonNull
    @Override
    public RecipeList2ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.suggested_recipe_list_2, parent, false);
        RecipeList2ViewHolder recipeList2ViewHolder = new RecipeList2ViewHolder(v);

        return recipeList2ViewHolder;
    }

    public interface OnClickListener {
        void onItemClick(int position);
    }

    @Override
    public int getItemCount() {
        return recipeList2.size();
    }

    public void filterRecipeList(ArrayList<RecipeModel> recipeList) {
        recipeList2 = recipeList;
        notifyDataSetChanged();
    }

    public class RecipeList2ViewHolder extends RecyclerView.ViewHolder {

        TextView suggestedRecipeList2;
        TextView tvRemainingIngredients;
        ImageView imgvIngredientList2;

        public RecipeList2ViewHolder(@NonNull View itemView) {
            super(itemView);
            suggestedRecipeList2 = itemView.findViewById(R.id.tvSuggestedRecipeList2);
            tvRemainingIngredients = itemView.findViewById(R.id.tvRemainingIngredients);
            imgvIngredientList2 = itemView.findViewById(R.id.imgvIngredientList2);
        }
    }
}