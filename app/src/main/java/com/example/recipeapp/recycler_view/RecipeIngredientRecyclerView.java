package com.example.recipeapp.recycler_view;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.recipeapp.R;

import java.util.ArrayList;

public class RecipeIngredientRecyclerView extends RecyclerView.Adapter<RecipeIngredientRecyclerView.RecipeIndredientViewHolder> {

    ArrayList<String> ingredientName;
    ArrayList<String> unitOfMeasureName;
    ArrayList<String> unitOfMeasureQuantity;

    public RecipeIngredientRecyclerView(ArrayList<String> ingredientName,
                                        ArrayList<String> unitOfMeasureName,
                                        ArrayList<String> unitOfMeasureQuantity) {
        this.ingredientName = ingredientName;
        this.unitOfMeasureName = unitOfMeasureName;
        this.unitOfMeasureQuantity = unitOfMeasureQuantity;
    }

    @NonNull
    @Override
    public RecipeIngredientRecyclerView.RecipeIndredientViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_recipe_ingredient_rcv,
                parent, false);
        RecipeIngredientRecyclerView.RecipeIndredientViewHolder recipeIndredientViewHolder = new RecipeIngredientRecyclerView.RecipeIndredientViewHolder(v);
        return recipeIndredientViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecipeIngredientRecyclerView.RecipeIndredientViewHolder holder, int position) {
        String currentIngredientName = ingredientName.get(position);
        String currentUOMName = unitOfMeasureName.get(position);
        String currentItemUOMQuantity = unitOfMeasureQuantity.get(position);

        String temp = currentItemUOMQuantity + " " + currentUOMName + " " + currentIngredientName;

        holder.cbForIngredient.setText(temp);
    }

    @Override
    public int getItemCount() {
        return ingredientName.size();
    }

    public class RecipeIndredientViewHolder extends RecyclerView.ViewHolder {

        CheckBox cbForIngredient;

        public RecipeIndredientViewHolder(@NonNull View itemView) {
            super(itemView);
            cbForIngredient = itemView.findViewById(R.id.cbForIngredient);
        }
    }
}
