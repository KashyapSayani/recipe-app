package com.example.recipeapp.recycler_view;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.recipeapp.R;
import com.example.recipeapp.model.RecipeModel;

import java.util.ArrayList;

public class SuggestedRecipeList1RecyclerView extends RecyclerView.Adapter<SuggestedRecipeList1RecyclerView.RecipeList1ViewHolder> {

    ArrayList<RecipeModel> recipeList1;
    private OnClickListener onClickListener;

    public SuggestedRecipeList1RecyclerView(ArrayList<RecipeModel> recipeList1, OnClickListener onClickListener) {
        this.recipeList1 = recipeList1;
        this.onClickListener = onClickListener;
    }

    @Override
    public void onBindViewHolder(@NonNull RecipeList1ViewHolder holder, int position) {
        RecipeModel currentItem = recipeList1.get(position);
        holder.suggestedRecipeList1.setText(currentItem.getRecipeName());

        holder.imgvIngredientList1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onClickListener != null) {
                    onClickListener.onItemClick(position);
                }
            }
        });

        holder.suggestedRecipeList1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickListener.onItemClick(position);
            }
        });
    }


    @NonNull
    @Override
    public RecipeList1ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.suggested_recipe_list_1, parent, false);
        RecipeList1ViewHolder recipeList1ViewHolder = new RecipeList1ViewHolder(v);

        return recipeList1ViewHolder;
    }

    public interface OnClickListener {
        void onItemClick(int position);
    }

    @Override
    public int getItemCount() {
        return recipeList1.size();
    }

    public class RecipeList1ViewHolder extends RecyclerView.ViewHolder {

        TextView suggestedRecipeList1 = itemView.findViewById(R.id.tvSuggestedRecipeList1);
        ImageView imgvIngredientList1 = itemView.findViewById(R.id.imgvIngredientList1);

        public RecipeList1ViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }

    public void filterRecipeList(ArrayList<RecipeModel> filteredRecipeList) {
        recipeList1 = filteredRecipeList;
        notifyDataSetChanged();
    }
}