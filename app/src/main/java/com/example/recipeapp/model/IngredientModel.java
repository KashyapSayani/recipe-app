package com.example.recipeapp.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class IngredientModel implements Serializable {
    Integer IngredientId;
    String IngredientName;
    Integer SelectedOrNot = 0;
}