package com.example.recipeapp.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RecipeModel implements Serializable {
    Integer RecipeId;
    String RecipeName;
    String RecipeImage;
    String RecipeMethod;
    String RecipeCookTime;
    String RecipePrepTime;
    String RecipeUrl;
    Integer CategoryId;
}
