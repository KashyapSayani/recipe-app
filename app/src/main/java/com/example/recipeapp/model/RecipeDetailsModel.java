package com.example.recipeapp.model;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RecipeDetailsModel implements Serializable {

    String RecipeName;
    String RecipeCookTime;
    String RecipePrepTime;
    ArrayList<String> UnitOfMeasureName;
    ArrayList<String> UnitOfMeasureQuantity;
    ArrayList<String> recipeWiseIngredient;
    String RecipeMethod;
}
