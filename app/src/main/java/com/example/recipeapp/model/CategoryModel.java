package com.example.recipeapp.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CategoryModel implements Serializable {
    Integer CategoryId;
    String CategoryName;
}