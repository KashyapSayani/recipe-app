package com.example.recipeapp.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RecipeWiseIngredientsModel implements Serializable {
    String RecipeId;
    String IngredientId;
    String UnitOfMeasureId;
    String UnitOfMeasureQuantity;

}
